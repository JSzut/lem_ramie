#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <iomanip>

void joyMessageReceived(const sensor_msgs::Joy &msg){
    //ROS_INFO_STREAM(std::setprecision(2) << std::fixed << "Position=("<<msg.axes<<" , "<<msg.axes<<")"<<" direction="<<msg.axes);
    //ROS_INFO_STREAM("Seq= "<<msg.seq);
    //for(int i = 0; i<msg.axes.size(); i++){
        ROS_INFO_STREAM(std::setprecision(2)<<std::fixed<<"Axes = ("<<msg.axes[0]<<","<<msg.axes[1]<<","<<msg.axes[2]<<","<<msg.axes[3]<<","<<msg.axes[4]<<","<<msg.axes[5]<<")");
        ROS_INFO_STREAM("Buttons: "<<msg.buttons[0]<<","<<msg.buttons[1]);
    //}
    
}
int main(int argc, char **argv){
    ros::init(argc, argv, "lem_spacemouse_subscriber");
    ros::NodeHandle nh;

    ros::Subscriber sub = nh.subscribe("spacenav/joy", 1000, &joyMessageReceived);
    ros::spin();
}